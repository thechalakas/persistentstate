package com.thechalakas.jay.persistentstage;

/*
 * Created by jay on 16/09/17. 1:39 AM
 * https://www.linkedin.com/in/thesanguinetrainer/
 */

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.i("MainActivity","onCreate reacbed");

        //button that will set some preferences
        Button button = (Button) findViewById(R.id.button);

        button.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {

                Log.i("MainActivity","button onCreate onClick reached");
                // We need an Editor object to make preference changes.
                // All objects are from android.context.Context
                SharedPreferences settings = getSharedPreferences("preferencesfile", 0);
                SharedPreferences.Editor editor = settings.edit();
                //editor.putBoolean("silentMode", mSilentMode);
                //key is name1
                //value is Vijayasimha BR
                editor.putString("name1","Vijayasimha BR");

                // Commit the edits!
                editor.commit();

                Log.i("MainActivity","Name has been saved!");
            }
        });

        Button buttonGotoNextPage = (Button) findViewById(R.id.button3);

        buttonGotoNextPage.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Log.i("MainActivity","buttonGotoNextPage onCreate onClick reached");
                //go to the next activity and see if you can load the name shared preferences
                Intent intent = new Intent(getApplicationContext(),Main2Activity.class);
                startActivity(intent);
            }
        });

        Log.i("MainActivity","onCreate Ended");
    }
}
