package com.thechalakas.jay.persistentstage;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class Main2Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        Log.i("Main2Activity","onCreate Reached");

        //button that will set some preferences
        Button button = (Button) findViewById(R.id.button2);

        button.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {

                Log.i("Main2Activity","onCreate onClick reached");
                // Restore preferences
                SharedPreferences settings = getSharedPreferences("preferencesfile", 0);
                //boolean silent = settings.getBoolean("silentMode", false);
                String name_recovered = settings.getString("name1","name not found");
                Log.i("Main2Activity","Name is - "+name_recovered);
                Log.i("Main2Activity","Name has been recovered!");
            }
        });

        Log.i("Main2Activity","onCreate Ended");
    }
}
